package demo.drools.app;

import java.util.HashMap;

import org.drools.KnowledgeBase;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.StatelessKnowledgeSession;

import demo.drools.model.Account;

/**
 * Hello world!
 *
 */
public class SimpleDemo {
  public static void main(String[] args) {
    demoWithBpmn();

    demoWithDrl();

  }

  private static void demoWithDrl() {
    System.out.println("-------------------Magic with Drl File starts---------------------");
    KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
    kbuilder.add(ResourceFactory.newClassPathResource("account.drl"), ResourceType.DRL);
    KnowledgeBase kbase = kbuilder.newKnowledgeBase();
    StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();

    for (int i = 10; i >= 1; i--) {
      Account account = new Account(200 * i);
      account.withdraw(150);
      ksession.execute(account);
    }

    System.out.println("-------------------Magic with Drl File ends---------------------");

  }

  private static void demoWithBpmn() {
    System.out.println("-------------------Magic with Bpmn File starts---------------------");
    KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
    kbuilder.add(ResourceFactory.newClassPathResource("process/demo.bpmn"), ResourceType.BPMN2);
    KnowledgeBase kbase = kbuilder.newKnowledgeBase();
    StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();

    HashMap<String, Object> params = new HashMap<String, Object>();
    params.put("name", "Steffi");

    ksession.startProcess("greeting.process", params);
    ksession.dispose();

    System.out.println("-------------------Magic with Bpmn File ends---------------------");

  }
}
