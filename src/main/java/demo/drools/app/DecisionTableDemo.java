package demo.drools.app;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import demo.drools.model.Customer;

public class DecisionTableDemo {

    public static void main(String[] args) {
        try {

            // load up the knowledge base
            KieServices ks = KieServices.Factory.get();
            KieContainer kContainer = ks.getKieClasspathContainer();
            KieSession kSession = kContainer.newKieSession("ksession-dtables");
            for (int i = 0; i < 121; i += 10) {
                Customer customer = new Customer("Tester", "Age" + i, i, "NO");
                kSession.insert(customer);
                kSession.fireAllRules();
                System.out.println(customer.toString());
            }

            Customer customer = new Customer("Tester", "Age" + 10, 10, "YES");
            kSession.insert(customer);
            kSession.fireAllRules();

            System.out.println(customer.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
