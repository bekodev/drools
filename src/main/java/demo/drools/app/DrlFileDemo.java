package demo.drools.app;

import java.util.ArrayList;
import java.util.List;

import org.drools.KnowledgeBase;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;

import demo.drools.model.Car;

public class DrlFileDemo {

    public static void main(String[] args) {
        System.out.println("-------------------Start Car Demo with List---------------------");
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ResourceFactory.newClassPathResource("rules/car.drl"), ResourceType.DRL);
        KnowledgeBase kbase = kbuilder.newKnowledgeBase();
        StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
        List<String> carList = new ArrayList<String>();
        ksession.setGlobal("myCarList", carList);

        Car car = new Car(2012, "blau", "Audi");
        ksession.execute(car);

        Car car1 = new Car(2011, "grün", "VW");
        ksession.execute(car1);

        Car car2 = new Car(2001, "lila", "BMW");
        ksession.execute(car2);

        Car car3 = new Car(1999, "schwarz", "Seat");
        ksession.execute(car3);

        carList.stream().forEach(s -> System.out.println(s));

        System.out.println("-------------------End Car Demo with List---------------------");

    }

}
