package demo.drools.services;

public class CarServices {

    // Method must be static
    public static void thisMethodDoesSomething(String farbe) {
        System.out.println("Print something out of a Java-Class. Farbe: " + farbe);
    }
}
