package demo.drools.model;

import lombok.Getter;
import lombok.Setter;

public class Customer {

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    private int age;

    @Getter
    @Setter
    private int flag;

    @Getter
    @Setter
    private int price;

    @Getter
    @Setter
    private String risk;

    public Customer(String firstName, String lastName, int age, String risk) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.risk = risk;
    }

    @Override
    public String toString() {
        return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + ", flag=" + flag + ", risk=" + risk + ", price=" + price + "]";
    }

}
