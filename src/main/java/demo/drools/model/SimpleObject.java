package demo.drools.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class SimpleObject {

  @Getter
  @Setter
  private int zahl;


}
