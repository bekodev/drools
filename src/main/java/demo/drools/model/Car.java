package demo.drools.model;

public class Car {

    private Integer baujahr;
    private String farbe;
    private String marke;
    
    
    
    public Car(Integer baujahr, String farbe, String marke) {
        super();
        this.baujahr = baujahr;
        this.farbe = farbe;
        this.marke = marke;
    }
    
    
    public Integer getBaujahr() {
        return baujahr;
    }
    public void setBaujahr(Integer baujahr) {
        this.baujahr = baujahr;
    }
    public String getFarbe() {
        return farbe;
    }
    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }
    public String getMarke() {
        return marke;
    }
    public void setMarke(String marke) {
        this.marke = marke;
    }


    @Override
    public String toString() {
        return "Car [baujahr=" + baujahr + ", farbe=" + farbe + ", marke=" + marke + "]";
    }
    
    
    
    
    
}
