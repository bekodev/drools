package demo;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import demo.drools.model.SimpleObject;

public class ExampleRuleTest extends RuleTest {

  private List<String> invalidRules;

  @Override
  @Test
  public void testInvalid() throws Exception {
    SimpleObject so = new SimpleObject(0);
    Map<String, Object> globals = setUpGlobals();

    executeRule(so, "junit_example.drl", "REGELGRUPPE", globals);
    System.out.print(invalidRules.size());
    Assert.assertTrue(invalidRules.size() == 1);
  }

  @Override
  @Test
  public void testValid() throws Exception {
    SimpleObject so = new SimpleObject(1);

    Map<String, Object> globals = setUpGlobals();

    executeRule(so, "junit_example.drl", "REGELGRUPPE", globals);
    System.out.print(invalidRules.size());
    Assert.assertTrue(invalidRules.size() == 0);
  }

  private Map<String, Object> setUpGlobals() {
    Map<String, Object> globals = Maps.newHashMap();
    invalidRules = Lists.newArrayList();
    globals.put("invalidRules", invalidRules);
    return globals;
  }

}
