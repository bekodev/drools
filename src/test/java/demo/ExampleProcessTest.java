package demo;

import org.jbpm.test.JbpmJUnitBaseTestCase;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;

public class ExampleProcessTest extends JbpmJUnitBaseTestCase {

  @Test
  public void testProcess() {
    createRuntimeManager("process/process_test.bpmn");
    RuntimeEngine runtimeEngine = getRuntimeEngine();
    KieSession ksession = runtimeEngine.getKieSession();

    ProcessInstance processInstance = ksession.startProcess("com.sample.bpmn.hello");
    // check whether the process instance has completed successfully
    assertProcessInstanceNotActive(processInstance.getId(), ksession);
    assertNodeTriggered(processInstance.getId(), "Start", "Hello", "End");
  }

}
