package demo;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.drools.KnowledgeBase;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.junit.Test;

public abstract class RuleTest {

  public void executeRule(Object meldung, String ruleResource, String agendaGroup,
      Map<String, Object> globals) {
    KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
    kbuilder.add(ResourceFactory.newClassPathResource(ruleResource), ResourceType.DRL);

    KnowledgeBase kbase = kbuilder.newKnowledgeBase();
    StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
    if (StringUtils.isNotBlank(agendaGroup)) {
      ksession.getAgenda().getAgendaGroup(agendaGroup).setFocus();
    }
    for (Map.Entry<String, Object> entry : globals.entrySet()) {
      ksession.setGlobal(entry.getKey(), entry.getValue());
    }
    ksession.insert(meldung);
    ksession.fireAllRules();
  }

  @Test
  public abstract void testInvalid() throws Exception;

  @Test
  public abstract void testValid() throws Exception;
}
